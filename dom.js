(function() {
    function addUsersToDom(user) {
        var node = document.createElement('li')
        var text = document.createTextNode(user)
        node.appendChild(text)
    
        document.getElementById('users').appendChild(node)
    }
    
    function addUsersFromInput() {
        var input = document.getElementById('input')
        addUsersToDom(input.value)
        input.value = ''
        input.focus()
    }
    
    document.getElementById('submit').addEventListener('click', addUsersFromInput)
    
    var users = APP.getUsers()
    for (var i = 0; i < users.length; i++) {
        addUsersToDom(users[i])
    }
})()

